from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] =  os.path.join(BASE_DIR, '..',
                                                             "TwitterSentimentAnalysis-113cdb24add5.json")

CORS_ORIGIN_WHITELIST = (
   'http://twitter-sentiment-analysis2.herokuapp.com',
)

DATABASES = {}

ALLOWED_HOSTS = ['django-env.gqpastpgwd.eu-west-3.elasticbeanstalk.com']

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, '..', "static"),
)

STATIC_ROOT = os.path.join(BASE_DIR, '..', 'static_collected')
