"""
This is a very simple script for testing the Django API for analyzing tweet sentiments.
"""
import datetime
import requests


def example_request():
    """
    Send a sample of tweets to the server and print the response
    """

    data = {
        "tweets": [
            {
                "author": "gossminn",
                "date_time": datetime.datetime.now().timestamp(),
                "content": "Hello world! I love organic cookies!"
            },
            {
                "author": "gossminn",
                "date_time": datetime.datetime.now().timestamp(),
                "content": "I would really like to buy this product!"
            },
            {
                "author": "gossminn",
                "date_time": datetime.datetime.now().timestamp(),
                "content": "I really hate this product, it stopped working after only one day of using it."
            },
            {
                "author": "gossminn",
                "date_time": datetime.datetime.now().timestamp(),
                "content": "I hate myself!"
            }
        ]
    }

    r = requests.get("http://127.0.0.1:8000/tweet_sentiment/sentiment", json=data)

    print(r.text)


if __name__ == "__main__":
    example_request()
