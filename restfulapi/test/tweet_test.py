"""
This is a very simple script for testing our Django API for retrieving tweets.
"""

import requests


def example_request():
    """
    Send to sample queries to the server and print the responses
    """

    data = [
        {
            'type': 'hashtag',
            'content': '#blocus'
        },
        {
            'type': 'user',
            'content': '@realdonaldtrump'
        }
    ]

    for query in data:
        r = requests.get("http://127.0.0.1:8000/tweet_sentiment/tweets", json=query)
        print(r.text)


if __name__ == "__main__":
    example_request()
