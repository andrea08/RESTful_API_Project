import twitter
from datetime import datetime
from tweet_sentiment.data_structures import Tweet, TweetList


api = twitter.Api(consumer_key='o6U3cEE0EMijpPwlgSBWAgNmu',
                  consumer_secret='hN8GnKtNZLaBkOKNfHDCzQvFLevM2Q7wEXbtrXziUgBsewD0T8',
                  access_token_key='843234589-bE3dyAcErm6WenxYLFi2W3Z4j8eAeciuUIH8Lnvs',
                  access_token_secret='BM4OPTvwIKAfo8LbKRvvojrN0ULTxrPhnFiX8Mfcawiwx', tweet_mode='extended')

def string_date_to_float(date):
    date_from_string = datetime.strptime(date, '%a %b %d %H:%M:%S %z %Y')
    float_date = date_from_string.timestamp()
    return float_date

def get_tweets_by_hashtag(hashtag, count):
    hashtag = hashtag.lstrip("#")
    search = api.GetSearch(raw_query="q=%23" + hashtag + "&count=" + str(count))
    tweet_list = []
    for tweet in search:
        date = string_date_to_float(tweet.created_at)
        tweet_list.append(Tweet("@" + tweet.user.screen_name,  date, tweet.full_text))
    return TweetList(len(tweet_list), tweet_list)


def get_user_tweets(user, count):

    try:
        t = api.GetUserTimeline(screen_name=user, count=count)
        tweet_list = []
        tweets = [i.AsDict() for i in t]
        for t in tweets:
            date = string_date_to_float(t['created_at'])
            tweet_list.append(Tweet(user, date, t['full_text']))
    except:
        tweet_list = []
    return TweetList(len(tweet_list), tweet_list)


def run_example():
    #print( get_tweets_by_hashtag("fafdablocus", 3))
    print(get_user_tweets("@KRzdfadfLS",3))



if __name__ == "__main__":
    run_example()
