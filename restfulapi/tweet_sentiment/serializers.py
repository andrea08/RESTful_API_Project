"""
REST framework `Serializer`s for the data structures defined in `data_structures.py`
Classes support only serializing and deserializing, not updating existing objects
"""
import datetime
from django.conf import settings
from io import BytesIO

if __name__ == "__main__": settings.configure()  # needed to run the examples

from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework import serializers
from tweet_sentiment.data_structures import Score, TweetQuery, TweetList, Tweet, SentimentQuery, \
    SentimentData, Error


class TweetQuerySerializer(serializers.Serializer):
    """
    Class for (de)serializing a `TweetQuery` object
    """
    type = serializers.CharField()
    content = serializers.CharField()

    def create(self, validated_data):
        return TweetQuery(**validated_data)

    def update(self, instance, validated_data):
        raise NotImplementedError("Updating existing objects is not supported")


class TweetSerializer(serializers.Serializer):
    """
    Class for (de)serializing a `Tweet` object
    """
    author = serializers.CharField()
    date_time = serializers.FloatField()
    content = serializers.CharField()

    def create(self, validated_data):
        return Tweet(**validated_data)

    def update(self, instance, validated_data):
        raise NotImplementedError("Updating existing objects is not supported")


class TweetListSerializer(serializers.Serializer):
    """
    Class for (de)serializing a `TweetList` object
    """
    number_found = serializers.IntegerField()
    tweets = TweetSerializer(many=True)

    def create(self, validated_data):
        tweets_data = validated_data.pop("tweets")
        tweet_list = TweetList(tweets=[], **validated_data)

        for tweet_data in tweets_data:
            tweet = Tweet(**tweet_data)
            tweet_list.tweets.append(tweet)

        return tweet_list

    def update(self, instance, validated_data):
        raise NotImplementedError("Updating existing objects is not supported")


class SentimentQuerySerializer(serializers.Serializer):
    """
    Class for (de)serializing a `SentimentQuery` object
    """
    tweets = TweetSerializer(many=True)

    def create(self, validated_data):
        tweets_data = validated_data.pop("tweets")
        query = SentimentQuery(tweets=[])

        for tweet_data in tweets_data:
            tweet = Tweet(**tweet_data)
            query.tweets.append(tweet)

        return query

    def update(self, instance, validated_data):
        raise NotImplementedError("Updating existing objects is not supported")


class ScoreSerializer(serializers.Serializer):
    """
    Class for (de)serializing a `Score` object
    """
    positive_number = serializers.IntegerField()
    positive_percentage = serializers.FloatField()
    negative_number = serializers.IntegerField()
    negative_percentage = serializers.FloatField()
    neutral_number = serializers.IntegerField()
    neutral_percentage = serializers.FloatField()

    def create(self, validated_data):
        return Score(**validated_data)

    def update(self, instance, validated_data):
        raise NotImplementedError("Updating existing objects is not supported")


class SentimentDataSerializer(serializers.Serializer):
    """
    Class for (de)serializing a `SentimentData` object
    """
    number_found = serializers.IntegerField()
    skipped = serializers.IntegerField()
    score = ScoreSerializer(many=False)

    def create(self, validated_data):
        score_data = validated_data.pop("score")
        score = Score(**score_data)
        sentiment = SentimentData(**validated_data, score=score)
        return sentiment

    def update(self, instance, validated_data):
        raise NotImplementedError("Updating existing objects is not supported")


class MinMaxTweetsSerializer(serializers.Serializer):
    """
    Class for (de)serializing a `MinMaxTweets` object
    """
    min_score = serializers.FloatField()
    min_tweet = TweetSerializer(many=False)
    max_score = serializers.FloatField()
    max_tweet = TweetSerializer(many=False)

    def create(self, validated_data):
        min_tweet_data = validated_data.pop("min_tweet")
        max_tweet_data = validated_data.pop("max_tweet")
        min_tweet = Tweet(**min_tweet_data)
        max_tweet = Tweet(**max_tweet_data)
        return MinMaxTweetsSerializer(**validated_data, min_tweet=min_tweet, max_tweet=max_tweet)

    def update(self, instance, validated_data):
        raise NotImplementedError("Updating existing objects is not supported")


class ErrorSerializer(serializers.Serializer):
    """
    Class for (de)serializing an `Error` object
    """
    error_information = serializers.CharField()

    def create(self, validated_data):
        return Error(**validated_data)

    def update(self, instance, validated_data):
        raise NotImplementedError("Updating existing objects is not supported")


def run_examples():
    # Score: serialize
    score = Score(1, 50.0, 0, 0.0, 1, 50.0)
    serializer = ScoreSerializer(score)
    json_string = JSONRenderer().render(serializer.data)
    print(json_string)

    # Score: deserialize
    stream = BytesIO(json_string)
    data = JSONParser().parse(stream)
    serializer = ScoreSerializer(data=data)
    if serializer.is_valid():
        score2 = serializer.save()
        print(score2)

    # Tweet list: serialize
    tweets = [
        Tweet("gossminn", datetime.datetime.now().timestamp(), "Hello world!"),
        Tweet("RealDonaldTrump", datetime.datetime.now().timestamp(), "America First!")
    ]

    tweet_list = TweetList(2, tweets)

    serializer = TweetListSerializer(tweet_list)
    json_string = JSONRenderer().render(serializer.data)
    print(json_string)

    # Tweet list: deserialize
    stream = BytesIO(json_string)
    data = JSONParser().parse(stream)
    serializer = TweetListSerializer(data=data)
    if serializer.is_valid():
        print(serializer.save())


# Some examples/tests
if __name__ == "__main__":
    run_examples()
