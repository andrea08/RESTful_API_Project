"""
Functions for sending tweets to Google's API and processing the results
"""

import datetime
import time

from google.api_core.exceptions import InvalidArgument
from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types

from tweet_sentiment.data_structures import SentimentData, Score, Tweet, MinMaxTweets


def analyze_tweets(tweets):
    """
    Send a list of tweets to the Google API and return the processed results
    :param tweets: list of Tweet objects
    :return: (SentimentData * MinMaxTweets) tuple
    """
    start_time = time.time()
    # print("Timer started")

    client = language.LanguageServiceClient()
    # print("Time after connecting to Google Cloud server: {}".format(time.time() - start_time))

    annotations, skipped = collect_annotations(client, tweets)
    # print("Time after collecting annotations: {}".format(time.time() - start_time))

    if len(annotations) == 0:
        return SentimentData(0, skipped, Score(0, 0, 0, 0, 0, 0))

    counts = {
        "positive": 0,
        "negative": 0,
        "neutral": 0
    }

    for annotation in annotations:
        doc_score = annotation.document_sentiment.score
        if doc_score < -0.25:
            counts["negative"] += 1
        elif -0.25 <= doc_score > 0.25:
            counts["neutral"] += 1
        else:
            counts["positive"] += 1

    sentiment_data = SentimentData(len(annotations), skipped, Score(
        counts["positive"],
        counts["positive"] / len(annotations),
        counts["negative"],
        counts["negative"] / len(annotations),
        counts["neutral"],
        counts["neutral"] / len(annotations)
    ))

    # print("Time after calculating statistics: {}".format(time.time() - start_time))

    get_score = lambda x: x.document_sentiment.score
    most_postive = annotations.index(max(annotations, key=get_score))
    most_negative = annotations.index(min(annotations, key=get_score))

    min_max = MinMaxTweets(
        annotations[most_negative].document_sentiment.score,
        tweets[most_negative],
        annotations[most_postive].document_sentiment.score,
        tweets[most_postive]
    )

    # print("Time after finding extreme tweets: {}".format(time.time() - start_time))

    return sentiment_data, min_max


def collect_annotations(client, tweets):
    """
    Get Google Cloud sentiment annotations for a list of tweets
    :param client: Google Cloud Language client object
    :param tweets: list of Tweet objects
    :return: (list of annotations * number of tweets that were skipped) tuple
    """
    annotations = []
    count = 0
    skipped = 0
    for tweet in tweets:
        document = types.Document(
            content=tweet.content,
            type=enums.Document.Type.PLAIN_TEXT
        )
        try:
            start_time = time.time()
            count += 1
            annotations.append(client.analyze_sentiment(document=document))
            # print("Analyzing tweet #{} took {}s".format(count, time.time() - start_time))

        except InvalidArgument:
            skipped += 1
            count += 1
            # print("Skipped tweet #{}".format(count))
            continue

    return annotations, skipped


def run_example():
    """
    Simple example to demonstrate the functionality of this module
    """
    tweets = []
    with open("sample_tweets.txt") as f:
        for line in f.readlines():
            tweet = Tweet("BarackObama", datetime.datetime.now().timestamp(), line)
            tweets.append(tweet)

    print(analyze_tweets(tweets))


if __name__ == "__main__":
    run_example()
