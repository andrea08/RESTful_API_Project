Andrés Herrera Cepero, Gosse Minnema, Aria Nourbakhsh, María Andrea Cruz Blandón

A RESTful API (Django) and an SPA AngularJS application that uses the RESTful API.

***
# RESTful API

## 1. Methods Available

The RESTful API developed provides two methods one for retrieving tweets and the other one for analysing tweets.

### tweet_sentiment/tweets

This method allows to retrieve tweets using a hashtag or a nickname. To use this method you should send a json object
like the one in the following example:


```
tweet_query = {
                "type": "hashtag",
                "content": "#Dancing"
              }
```

Send the request using angular:

```
url = server + "/tweet_sentiment/tweets"
$http.post(url, tweet_query)
```

As a response the API will send a list of tweets which have the structure of the following example:

```
tweet_list = {
                "number_found": 3,
                "tweets": [
                    {
                        "author": "gossminn",
                        "date_time": '1522351047.353921',
                        "content": "Hello world! I love organic cookies!"
                    },
                    {
                        "author": "gossminn",
                        "date_time": '1522351047.353921',
                        "content": "I hate studying!"
                    },
                    {
                        "author": "user_1",
                        "date_time": '1522351047.353921',
                        "content": "My first tweet :)!"
                    }
                ]
             }
```

When no tweets were found the value for number_found will be 0 and the value for tweets will be an empty list

### tweet_sentiment/sentiment

This method allows to send a list of tweets to analyse with google-sentiment-analysis. To use this method you should send a
json object like the one in the following example:


```
sentiment_query_dev = {
                        "tweets": [
                            {
                                "author": "gossminn",
                                "date_time": '1522351047.353921',
                                "content": "Hello world! I love organic cookies!"
                            },
                            {
                                "author": "gossminn",
                                "date_time": '1522351047.353921',
                                "content": "I hate studying!"
                            }
                        ]
                      }

```

Send the request using angular:

```
url = server + "/tweet_sentiment/sentiment"
$http.post(url, sentiment_query)
```

As a response the API will send a sentiment_analysis json object with the analysis made by google.
The json object will have the structure of the following example:

```
sentiment_data = [{
                    "number_found":2,
                    "skipped":0,
                    "score":{
                        "positive_number":1,
                        "positive_percentage":0.5,
                        "negative_number":0,
                        "negative_percentage":0.0,
                        "neutral_number":1,
                        "neutral_percentage":0.5
                    }
                 },
                 {
                    "max_score": 0.8,
                    "max_tweet": {
                                    "author": "@helloWorld",
                                    "date_time": 1523802600,
                                    "content": "I am so happy today #Happy"
                                 }
                    "min_score": -0.6,
                    "min_tweet": {
                                    "author": "@aguy",
                                    "date_time": 1523802791,
                                    "content": "I hate waking up early. Who is #Happy ?"
                                 }
                 }]

```

***
# SPA AngularJS

The Angular application provides a Single Page Application (SPA) site which allows 
user to use the RESTful API through it.

Please make sure you have already installed the requirements to run this project.

## 1. Requirements
You will need ot install Node.js and the node package Bower to run the project.
Please follow the instructions in the official web sites:

1. [Node.js](https://nodejs.org/en/download/)
2. [Bower](https://bower.io/)

## 2. Run server

When you have already installed the dependencies you can run the project using
the following commands:


```
cd angular_ui_template
npm run dev
```

This will serve the SPA in your localhost. Make sure you are already running the 
RESTful Django project to be able to communicate both applications.

***
# RESTful Django project (Run dummy server)

## 1. Requirements

For developers, it will be required to install Postgres, python3 and Django as the minimal requirements for Django project.

Create the database:

```
$ psql -U postgres -h localhost
postgres=# create user restfulapi;
postgres=# alter role restfulapi encrypted password 'restfulapi';
postgres=# create database restfulapi owner restfulapi;
```

Python packages. The file requeriments.txt contains all the python dependencies to run the project. Each time a developer installs 
a new package it is **mandatory** to update requeriments.txt, to do so please execute the following command:

```
cd path_to_project
pip3 freeze > requirement.txt
```

To install the existing requirements, please execute the following command:

```
pip3 install -r requirements.txt
```

## 2. Run project

To run Django project follow these steps:

1. Run migrations commands (your settings file without py extension)

```
python3 manage.py makemigrations  --settings=restfulapi.settings.your_settings
python3 manage.py migrate  --settings=restfulapi.settings.your_settings
```

2. Run the dummy server

```
python3 manage.py runserver --settings=restfulapi.settings.your_settings
```

If you are using PyCharm you can specify your settings:

* First you need to enable Django support for the project:

1. File menu > Settings
2. Language & Frameworks > Django
3. Check Enable Django Support
4. Configure the project root path_to/RESTful_API_Project/restfulapi
5. Indicate your settings file
6. Click on Apply and after OK

* Run the server:

1. Run menu > Edit Configurations...
2. Button + > Django server
3. Name can be restfulapi or the name of your preference
4. In additional options type: --settings=restfulapi.settings.your_settings without the py extension
5. Click on Apply and after OK
6. Run server by clicking on play button

